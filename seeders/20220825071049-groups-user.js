'use strict';

module.exports = {
  up:(queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
    return queryInterface.bulkInsert('Groups', [{
      name: 'laptops',
      description:'A modern laptop is self-contained, with a screen, keyboard and pointing device (like a touchpad), plus usually, speakers, a microphone and a camera',
      isactive:'true',
      createdAt:new Date,
      updatedAt: new Date
    },
    {
      name: 'Mobiles',
      description:'A mobile phone is a wireless handheld device that allows users to make and receive calls',
      isactive:'true',
      createdAt:new Date,
      updatedAt: new Date
    },
    {
      name: 'Headsets',
      description:'A headset makes spoken communication possible without having to wear an earpiece or hold a microphone',
      isactive:'false',
      createdAt:new Date,
      updatedAt: new Date

}], {});
  },

  down:(queryInterface, Sequelize) =>  {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    return queryInterface.bulkDelete('Groups', null, {});
  }
};
