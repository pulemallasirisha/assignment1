var express = require('express');

var router = express.Router();

var groupscontroller = require('../controller/groupscontroller');

// /*GET home page.*/

router.get('/',groupscontroller.getAll);
router.get('/categories',groupscontroller.getAll);
router.get('/api/products/id',groupscontroller.getAll);
router.get('/withcategories',groupscontroller.getAllWithCategories);
router.get('/withproducts',groupscontroller.getAllWithProducts);
module.exports = router;